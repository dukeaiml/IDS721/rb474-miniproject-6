# rb474-miniproject-5

## Service
The Lambda function returns the population in the United States for a given year if such information is present in the database. The function is invoked by Gateway. You can follow the [link](https://ardn6nyp53.execute-api.us-west-1.amazonaws.com/population?year=2018) to test its functionality.

The project is an extension of [MiniProject 5](https://gitlab.com/dukeaiml/IDS721/rb474-miniproject-5).

## Requirements
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Process
1. Steps 1-5 from MiniProject 5.
2. Since I had the logging in the previous project as well, I didn't have to change anything here. Adding logging is a matter of adding tracing subscriber in the code.
3. Add permissions `AWSXRayDaemonWriteAccess` and `AWSXrayFullAccess` to the user configured for the function.
4. Enable X-Ray traces in the configuration.
5. Access the function to check if the trace was created.

## Screenshots
### Logs
![](images/log.png)

### Traces
![](images/trace.png)