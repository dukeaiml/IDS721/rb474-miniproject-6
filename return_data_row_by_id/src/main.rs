use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use aws_sdk_dynamodb::{Client, types::AttributeValue};

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let year = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("year"));

    // Create a message to return
    // Find the data in the DynamoDB database by accessing the population column
    let message = match year {
        Some(year) => {
            //create client with defaults() and not Client::new(Default::default())
            let config = aws_config::load_from_env().await;
            let client = Client::new(&config);
            let item = client
                .get_item()
                .table_name("CloudDukeMiniProject5")
                .key("year", AttributeValue::S(year.to_string(),),)
                .send()
                .await?;
            match &item.item {
                Some(item_map) => {
                    if let Some(AttributeValue::S(population)) = item_map.get("population") {
                        format!("The population in {} in the United States was {}.", year, population)
                    } else {
                        // Handle case where "year" key is not found or not a string
                        "Data for the year is not found or is not a string".to_string()
                    }
                }
                None => "No data found for the year".to_string(),
            }
        }
        None => "Please provide an id in the url as a query parameter".to_string(),
    };

    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(message.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
